/*
* Task 1: Object property getter
*/
function getProperty<Type>(a: Type, b: string|number): string|number {
    return a[b];
}

/*
* Task 2: Queue
*/
class Queue<Type> {
    elemtents: Array<Type> = []

    push(value: Type) {
        this.elemtents.push(value);
    }

    pop(): Type {
        return this.elemtents.shift();
    }

    getValue(): Array<Type> {
        return this.elemtents;
    }
}


export {Queue, getProperty};
